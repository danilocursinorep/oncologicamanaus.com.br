<?php /*
Plugin Name: WhatsApp + Contador de Cliques
Description: Lead de WhatsApp + contador de cliques simples.
Version: 1.0
Author: Danilo Cursino
Author URI: https://www.twitter.com/DaniloCursino */

function clique_scripts() {
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);

	wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/js/my-ajax-script.js', array('jquery') );
	wp_localize_script( 'ajax-script', 'myajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	wp_enqueue_script('whatscript', plugin_dir_url( __FILE__ ) . '/assets/js/script.js', array(), null, true);
	wp_enqueue_style( 'whatstyle', plugin_dir_url( __FILE__ ) . '/assets/css/style.css',false,'1.0','all');
}
add_action( 'wp_enqueue_scripts', 'clique_scripts' );


function custom_content_after_body_open_tag() { ?>
<div id="whatsApp">

  <div class="backshadow counter" data-cont="dark-bg, Fechar formulário WhatsApp"></div>

  <div class="whatsdiv">
      <div class="step step1 show">
          <div class="header">
              <img src="<?php echo plugin_dir_url( __FILE__ ); ?>/assets/img/whatsapp.png"> Oncológica Manaus | WhatsApp
          </div>
          <div class="body">
          	<script type="text/javascript" src="https://mkt.oncologicamanaus.com.br/form/generate.js?id=6"></script>
          </div>
      </div>
  </div>

  <div class="whatsIcon counter" data-cont="whatsapp-button, Botão WhatsApp">
      <img src="<?php echo plugin_dir_url( __FILE__ ); ?>/assets/img/whatsapp.png">
  </div>

</div>
<script type="text/javascript">
	if (typeof MauticFormCallback == 'undefined') {
    var MauticFormCallback = {};
	}
	MauticFormCallback['whatsappsite'] = {
		onResponse: function (response) {
			document.querySelector('#whatsApp .whatsIcon').classList.remove("hidden");
			document.querySelector('#whatsApp .whatsdiv').classList.remove("show");
			document.querySelector('#whatsApp .backshadow').classList.remove("show");
			setTimeout(function() {
				document.querySelector('#whatsApp .backshadow').classList.remove("open");
			}, 500);
			
			window.location.href = 'https://www.oncologicamanaus.com.br/redirecionando-whatsapp/';
		},
	};
</script>

<?php }
add_action( 'wp_body_open', 'custom_content_after_body_open_tag');


function array_de_contadores(){
	if ( get_option('todos_os_contadores') !== false ) {
		add_option('todos_os_contadores', array() , null, 'no');
	}
}
add_action('init', 'array_de_contadores');

function contadores(){
	$contador = $_POST['contador'];
	$nome = $_POST['nome'];
	if ( get_option( $contador ) !== false ) {
		$valor = intval(get_option($contador)) + 1;
		update_option($contador, $valor);
	} else {
		add_option($contador, 1 , null, 'no');
		$todos = get_option('todos_os_contadores');
		$todos[] = $contador;
		update_option('todos_os_contadores', $todos);
		if ($nome !== '') {
		add_option($contador . '_label', $nome , null, 'no');
			}
	}
}
add_action('wp_ajax_ajxcontador', 'contadores');   
add_action('wp_ajax_nopriv_ajxcontador', 'contadores');

function contadores_delete(){
	$contador = $_POST['contador'];
	delete_option($contador);
}
add_action('wp_ajax_ajxcontadordelete', 'contadores_delete');   
add_action('wp_ajax_nopriv_ajxcontadordelete', 'contadores_delete');

function contadores_zerar(){
	$contador = $_POST['contador'];
	update_option($contador, 0);
}
add_action('wp_ajax_ajxcontadorzerar', 'contadores_zerar');   
add_action('wp_ajax_nopriv_ajxcontadorzerar', 'contadores_zerar');

function contador_page() {
		add_menu_page (
		'Contador de cliques', // page <title>Title</title>
		'Cliques', // menu link text
		'manage_options', // capability to access the page
		'contador-de-cliques', // page URL slug
		'page_content', // callback function /w content
		'dashicons-star-half', // menu icon
		5 // priority
	);
}
add_action('admin_menu', 'contador_page');

function page_content(){ ?>
	<div class="wrap">
		<div class="contador">
			<div class="row">
				<?php $contadores = get_option('todos_os_contadores'); ?>
				<?php foreach ($contadores as $contador): ?>
					<div class="col col-3">
						<div class="container">
							<div class="conteudo">
								<h2><?php echo get_option($contador); ?></h2><h3><?php echo get_option($contador.'_label'); ?></h3>
							</div>
							<!--<div class="img">
							<img class="refresh" src="'.get_template_directory_uri().'/assets/images/admin/refresh.png" />
							<img class="close" src="'.get_template_directory_uri().'/assets/images/admin/remove.png" />
							</div>-->
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	
	<?php global $_wp_admin_css_colors; ?>
	<?php $admin_color = get_user_option( 'admin_color' ); ?>
	<?php $colors = $_wp_admin_css_colors[$admin_color]->colors; ?>

	<style rel='stylesheet' type='text/css'>
		.contador {
			margin: 50px 0px;
		}
		.contador .row {
			width: 100%;
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			margin-right: -15px;
			margin-left: -15px;
			justify-content: center;
		}
		.contador .col {
			position: relative;
			width: 100%;
			padding-right: 15px;
			padding-left: 15px;
		}
		.contador .container {
			user-select: none;
			background-color: <?php echo $colors[0]; ?>;
			border-radius: 500px;
			position: relative;
			width: 100%;
			margin-bottom: 15px;
			transition: 0.4s;
		}
		.contador .container:hover {
			background-color: <?php echo $colors[2]; ?>;
		}
		.contador .container:after {
			content: '';
			display: block;
			padding-bottom: 100%;
		}
		.container .conteudo {
			color: #FFF;
			position: absolute;
			width: 70%;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%)
		}
		.container .img {
			position: absolute;
			bottom: 20px;
			left: 50%;
			transform: translateX(-50%);
			opacity: 0;
			transition: 0.4s;
		}
		.container:hover .img {
			opacity: 1;
		}
		.container .img img {
			filter: invert(1);
			max-width: 12px;
		}
		.container .img img.refresh {
			margin-right: 5px;
		}
		.container .conteudo h2 {
			display: block;
			color: #FFF;
			font-size: 42px;
			margin: 0px !important;
			font-weight: 700;
			text-align: center;
			height: 35px;
		}      
		.container .conteudo h3 {
			display: block;
			text-align: center;
			font-size: 11px;
			margin: 0px !important;
			line-height: 11px;
			width: 100%;
			color: #FFF;
			font-weight: 400;
		}      
		.contador .col.col-3 {
			-ms-flex: 0 0 15%;
			flex: 0 0 15%;
			max-width: 15%;
		}
	</style>";
<?php }