$(document).ready(function(){
     $('[data-cont]').click(function(e){
          var cont = $(this).attr('data-cont').split(/,\s*/);
          var nome = cont[1];
          var cont = cont[0];
          if (cont != '') {
               $.ajax({
                    dataType: "json",
                    url: myajax.ajax_url,
                    type: 'POST',
                    data: {
                         action: 'ajxcontador',
                         nome: nome,
                         contador: cont
                    }
               })
          }
    });
     $('#whatsApp .whatsIcon, .whatstrigger').click(function() {
          $('#whatsApp .step2').removeClass('show');
          $('#whatsApp .step1').addClass('show');

          $('#whatsApp .whatsIcon').addClass('hidden');
          $('#whatsApp .whatsdiv').addClass('show');
          $('#whatsApp .backshadow').addClass('open');
          
          setTimeout(function() {
               $('#whatsApp .backshadow').addClass('show');
          }, 1);
     });

    $('#whatsApp .backshadow').click(function() {
        $('#whatsApp .whatsIcon').removeClass('hidden');
        $('#whatsApp .whatsdiv').removeClass('show');
        $('#whatsApp .backshadow').removeClass('show');
        setTimeout(function() {
            $('#whatsApp .backshadow').removeClass('open');
        }, 500);
    });

    // $('#whatsApp .step1 button').click(function() {
    //       $('#whatsApp .step1').removeClass('show');
    //       var cont = $(this).attr('data-cont').split(/,\s*/);
    //       var valor = cont[0];
    //       var nome = cont[1];
    //       nome = nome.replace('Botão ','');
    //       nome = nome.replace('Formulário de ','');
    //       $('#whatsApp .step2 .header span').html(nome);
    //       $('#mauticform_input_whatsappsite_especialidade').val(valor);
    //       $('#whatsApp .step2').addClass('show');
    //       $('#mauticform_input_whatsappsite_nome').focus();
    // });

     $('.colCards img, .btContato').click(function() {
          $('#whatsApp .whatsIcon').addClass('hidden');
          $('#whatsApp .whatsdiv').addClass('show');
          $('#whatsApp .backshadow').addClass('open');
          setTimeout(function() { $('#whatsApp .backshadow').addClass('show'); }, 1);
          var cont = $(this).attr('data-cont').split(/,\s*/);
          var valor = cont[0];
          var nome = cont[1];
          var valor = valor.replace('-footer','');
          nome = nome.replace('Botão ','');
          nome = nome.replace(' (Página de Contato)','');
          nome = nome.replace(' (Footer)','');
          $('#whatsApp .step2 .header span').html(nome);
          $('#mauticform_input_whatsappsite_especialidade').val(valor);
          $('#whatsApp .step1').removeClass('show');
          $('#whatsApp .step2').addClass('show');
          $('#mauticform_input_whatsappsite_nome').focus();
     });

    $('#whatsApp .step2 .header i').click(function() {
        $('#whatsApp .step2').removeClass('show');
        $('#whatsApp .step1').addClass('show');
    });
     
     $('#mauticform_input_whatsappsite_whatsapp').mask('(00) 00000-0000');
});