<?php
/**
 * Oncológica functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Oncológica
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'oncologica_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function oncologica_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Oncológica, use a find and replace
		 * to change 'oncologica' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'oncologica', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'oncologica' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'oncologica_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'oncologica_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function oncologica_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'oncologica_content_width', 640 );
}
add_action( 'after_setup_theme', 'oncologica_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function oncologica_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'oncologica' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'oncologica' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'oncologica_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function oncologica_scripts() {
	//jQuery	
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.6.0.min.js', array(), null, false);

	//Bootstrap
	wp_enqueue_script('bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js', array(), null, true);
	wp_enqueue_style( 'bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css',false,'1.1','all');

	//Slick Slider
	wp_enqueue_script('slick-slider-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', array(), null, true);
	wp_enqueue_style( 'slick-slider-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css',false,'1.1','all');

	//Anime.js
	wp_enqueue_script( 'anime-js', 'https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.1/anime.min.js', array(), false, true );
	wp_enqueue_script( 'inview-js', 'https://cdnjs.cloudflare.com/ajax/libs/protonet-jquery.inview/1.1.2/jquery.inview.min.js', array(), false, true );

	//jQuey Mask
	wp_enqueue_script('mask-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js', array(), null, true);

	//Vendor and Custom JS
	wp_enqueue_script('vendor-min-js', get_template_directory_uri() . '/assets/js/vendor.min.js', array(), null, true);
	wp_enqueue_script('custom-min-js', get_template_directory_uri() . '/assets/js/custom.min.js', array(), null, true);

	wp_enqueue_style( 'oncologica-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'oncologica-style', 'rtl', 'replace' );

	wp_enqueue_script( 'oncologica-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'oncologica_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//Custom Type Equipe
function postTypeEquipe() {
   register_post_type('equipe', array(
       'labels' => array(
           'name' => __('Equipe'),
           'singular_name' => __('Equipe')
       ),
       'public' => true,
       'menu_position' => 8,
       'show_ui' => true,
       'capability_type' => 'page',
       'hierarchical' => false,
       'has_archive' => false,
       'rewrite' => array('slug' => 'equipe'),
       'menu_icon' => 'dashicons-groups',
       'supports' => array('title', 'thumbnail', 'editor', 'custom-fields')
   ));
}
add_action('init', 'postTypeEquipe');

//Custom Type E-book
function postTypeEbook() {
   register_post_type('ebook', array(
       'labels' => array(
		'name' => __('E-books'),
		'singular_name' => __('E-book')
       ),
       'public' => true,
       'menu_position' => 5,
       'show_ui' => true,
       'capability_type' => 'page',
       'hierarchical' => false,
       'has_archive' => false,
       'rewrite' => array('slug' => 'ebooks'),
       'menu_icon' => 'dashicons-book-alt',
       'supports' => array('title', 'thumbnail', 'editor')
   ));
}
add_action('init', 'postTypeEbook');


function pagination($query) {
	$total = $query->max_num_pages;
	$big = 999999999999999999;
	if ($total > 1) {
		$current = max(1, get_query_var('paged'));
		echo paginate_links(array(
			'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
			'format' => '?paged=%#%',
			'current' => $current,
			'prev_text' => '',
			'next_text' => '',
			'total' => $total,
		));
	}
}

function search_only_blog_posts( $query ) {

    if ( $query->is_search ) {
        $query->set('post_type', array('post', 'page', 'ebook'));
    }
    return $query;
}
add_filter( 'pre_get_posts','search_only_blog_posts' );

function document_title_separator( $sep ) {
    $sep = "|";
    return $sep;
}
add_filter( 'document_title_separator', 'document_title_separator' );


function my_body_classes($classes) {
	if (is_page() && !is_home()) {
		global $post;
		$slug = $post->post_name;
	    $classes[] = $slug;
	}
	return $classes;
}
add_filter( 'body_class','my_body_classes' );

function SearchFilter($query) {
    if ($query->is_search) {
        $query->set('post_type',array('post', 'equipe'));
    }
    return $query;
}
add_filter('pre_get_posts','SearchFilter');

function autores() {
	if ( function_exists( 'coauthors_posts_links' ) ) {
    	coauthors_posts_links(', ',' e ');
	} else {
	    the_author_posts_links(', ',' e ');
	}

}