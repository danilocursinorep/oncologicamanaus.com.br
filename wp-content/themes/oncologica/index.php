<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Oncológica
 */

get_header('home'); ?>

	<section class="agendamentoHome">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-md-6 align-self-center anima left">
					<h2>Agen<br>de</h2>
					<p>Queremos cuidar de você. Preencha seus dados ao lado e a nossa equipe entrará em contato para confirmar o agendamento de sua consulta.</p>
				</div>
				<div class="col-12 col-md-4 align-self-center anima right">
					<div class="formulario">
						<img class="agende agende1" alt="Agende" title="Agende" src="<?php echo get_template_directory_uri(); ?>/assets/img/imgAgende.png" />
						<div id="mauticform_wrapper_agendamentosite" class="mauticform_wrapper">
						     <form autocomplete="false" role="form" method="post" action="https://mkt.oncologicamanaus.com.br/form/submit?formId=1" id="mauticform_agendamentosite" data-mautic-form="agendamentosite" enctype="multipart/form-data">
						          <div class="mauticform-error" id="mauticform_agendamentosite_error"></div>
						          <div class="mauticform-message" id="mauticform_agendamentosite_message"></div>
						          <div class="mauticform-innerform">
						               <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">
						                    <div id="mauticform_agendamentosite_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
						                         <input id="mauticform_input_agendamentosite_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
						                         <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						                    </div>
						                    <div id="mauticform_agendamentosite_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
						                         <input id="mauticform_input_agendamentosite_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
						                         <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						                    </div>
						                    <div id="mauticform_agendamentosite_celular1" data-validate="celular1" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
						                         <input id="mauticform_input_agendamentosite_celular1" name="mauticform[celular1]" value="" placeholder="Celular" class="mauticform-input celular" type="text">
						                         <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						                    </div>
						                    <div id="mauticform_agendamentosite_submit" class="mauticform-row mauticform-button-wrapper mauticform-field-4">
						                         <input type="submit" name="mauticform[submit]" id="mauticform_input_agendamentosite_submit" value="" class="mauticform-button btn btn-default">
						                    </div>
						               </div>
						          </div>
						          <input type="hidden" name="mauticform[formId]" id="mauticform_agendamentosite_id" value="1">
						          <input type="hidden" name="mauticform[return]" id="mauticform_agendamentosite_return" value="">
						          <input type="hidden" name="mauticform[formName]" id="mauticform_agendamentosite_name" value="agendamentosite">
						     </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="especialidades">
		<div class="container">
			<div class="row justify-content-start anima left">
				<div class="col-12 col-lg-9">
					<div class="destaques">
						<div class="circulo"></div>

						<div class="destaque esq">
							<a href="<?php echo home_url('carcinoma-basocelular'); ?>">
								<img class="deston esq" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest1-carcinomabasocelular-selecionado.png" />
								<img class="dest" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest1-carcinomabasocelular.png" />
								<span>carcinoma <strong>basocelular</strong></span>
							</a>
						</div><!-- 
						 --><div class="destaque dir">
						 	<a href="<?php echo home_url('cancer-do-colo-do-utero-cervical'); ?>">
								<img class="deston dir" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest2-cancernocolodoutero-selecionado.png" />
								<img class="dest" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest2-cancernocolodoutero.png" />
								<span>câncer <strong>no colo<br> do útero</strong><strong>(cervical)</strong></span>
							</a>
						</div>

					</div>
				</div>
			</div>
			<div class="row justify-content-end anima right">
				<div class="col-12 col-lg-9">
					<div class="destaques">
						<div class="circulo"></div>

						<div class="destaque esq">
							<a href="<?php echo home_url('carcinoma-espinocelular'); ?>">
								<img class="deston esq" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest3-carcinomaespinocelular-selecionado.png" />
								<img class="dest" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest3-carcinomaespinocelular.png" />
								<span>carcinoma <strong>espinocelular</strong></span>
							</a>
						</div><!-- 
						 --><div class="destaque dir">
						 	<a href="<?php echo home_url('cancer-de-reto'); ?>">
								<img class="deston dir" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest4-carcinomaespinocelular-selecionado.png" />
								<img class="dest" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest4-carcinomaespinocelular.png" />
								<span>câncer <strong>no reto</strong></span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row justify-content-start anima left">
				<div class="col-12 col-lg-9">
					<div class="destaques esq">
						<div class="circulo"></div>

						<div class="destaque esq">
							<a href="<?php echo home_url('melanoma-maligno'); ?>">
								<img class="deston esq" class="destaque" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest5-melanomamaligno-selecionado.png" />
								<img class="dest" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest5-melanomamaligno.png" />
								<span>melanoma <strong>maligno</strong></span>
							</a>
						</div><!-- 
						 --><div class="destaque dir">
						 	<a href="<?php echo home_url('cancer-de-ovario'); ?>">
								<img class="deston dir" class="destaque" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest6-cancerdeovario-selecionado.png" />
								<img class="dest" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest6-cancerdeovario.png" />
								<span>câncer de <strong>ovário</strong></span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row justify-content-end anima right">
				<div class="col-12 col-lg-9">
					<div class="destaques">
						<div class="circulo"></div>

						<div class="destaque esq">
							<a href="<?php echo home_url(); ?>">
								<img class="deston esq" class="destaque" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest7-cancerdemama-selecionado.png" />
								<img class="dest" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest7-cancerdemama.png" />
								<span>câncer de <strong>mama</strong></span>
							</a>
						</div><!-- 
						 --><div class="destaque dir">
						 	<a href="<?php echo home_url(); ?>">
								<img class="deston dir" class="destaque" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest8-cancerdeprostata-selecionado.png" />
								<img class="dest" src="<?php echo get_template_directory_uri(); ?>/assets/img/dest8-cancerdeprostata.png" />
								<span>câncer de <strong>próstata</strong></span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="agendamentoHome">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-md-6 align-self-center anima left">
					<h2>Agen<br>de</h2>
					<p>Queremos cuidar de você. Preencha seus dados ao lado e a nossa equipe entrará em contato para confirmar o agendamento de sua consulta.</p>
				</div>
				<div class="col-12 col-md-4 align-self-center anima right">
					<div class="formulario">
						<img class="agende agende2" alt="Agende" title="Agende" src="<?php echo get_template_directory_uri(); ?>/assets/img/imgAgende2.png" />

						<div id="mauticform_wrapper_agendamento2site" class="mauticform_wrapper">
						    <form autocomplete="false" role="form" method="post" action="https://mkt.oncologicamanaus.com.br/form/submit?formId=5" id="mauticform_agendamento2site" data-mautic-form="agendamento2site" enctype="multipart/form-data">
						        <div class="mauticform-error" id="mauticform_agendamento2site_error"></div>
						        <div class="mauticform-message" id="mauticform_agendamento2site_message"></div>
						        <div class="mauticform-innerform">

						            
						          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

						            <div id="mauticform_agendamento2site_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
						                <input id="mauticform_input_agendamento2site_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_agendamento2site_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
						                <input id="mauticform_input_agendamento2site_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_agendamento2site_celular1" data-validate="celular1" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
						                <input id="mauticform_input_agendamento2site_celular1" name="mauticform[celular1]" value="" placeholder="Celular" class="mauticform-input celular" type="text">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_agendamento2site_submit" class="mauticform-row mauticform-button-wrapper mauticform-field-4">
						            	<input type="submit" name="mauticform[submit]" id="mauticform_input_agendamentosite_submit" value="" class="mauticform-button btn btn-default">
						            </div>
						            </div>
						        </div>

						        <input type="hidden" name="mauticform[formId]" id="mauticform_agendamento2site_id" value="5">
						        <input type="hidden" name="mauticform[return]" id="mauticform_agendamento2site_return" value="">
						        <input type="hidden" name="mauticform[formName]" id="mauticform_agendamento2site_name" value="agendamento2site">

						        </form>
						</div>

					
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="equipeHome">
		<div class="container">
			<div class="row full">
				<div class="col-12 col-lg-7 align-self-center text-left anima left">
					<div class="imageLG"></div>
				</div>
				<div class="col-12 col-lg-5 align-self-center anima right">
					<h3 class="tituloLG">nossa<br> equipe</h3>
					<h4>quer cuidar<br> de você</h4>
					<p>Conheça todo nosso time multicisciplinar e a nossa clínica para o atendimento médico que você precisa</p>
					<a href="<?php echo home_url('equipe'); ?>">
						<button>Nossos<br> especialistas</button>
					</a>
				</div>
			</div>
		</div>
	</section>

     <?php $queryPost = new WP_Query(array( 
          'post_type' => 'post',
          'posts_per_page' => 3,
          'order' => 'DESC'
     )); ?>
     <?php if($queryPost->have_posts()): ?>
	<section class="blogHome">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-10 align-self-center text-left">
					
					<div class="row justify-content-center">

						<div class="col-12 col-md-9 text-center">
							<h3 class="tituloLG dir anima right">Blog</h3>
							<h3 class="tituloLG esq anima left">Post</h3>
						</div>
					</div>

					<div class="itens row justify-content-center anima">

                    	<?php while($queryPost->have_posts()): $queryPost->the_post(); ?>
                    		<?php $index = $queryPost->current_post + 1; ?>
						<div class="item item<?php echo $index; ?> col-12 col-md-3">
							<a href="<?php the_permalink(); ?>">
								<div class="img">
									<?php the_post_thumbnail(); ?>
								</div>
								<h2><?php the_title(); ?></h2>
							</a>
						</div>
                    	<?php endwhile; ?>
						<div class="col-12 col-md-9 anima">
							<a href="<?php echo home_url('blog'); ?>">
								<button>Conheça todos os blogposts <img src="<?php echo get_template_directory_uri(); ?>/assets/img/setaSubmit.png" /></button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
     <?php endif; ?>

     <?php $queryBook = new WP_Query(array( 
          'post_type' => 'ebook',
          'posts_per_page' => 3,
          'order' => 'DESC'
     )); ?>
     <?php if($queryBook->have_posts()): ?>
	<section class="ebookHome">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 align-self-center anima">
					<h3 class="tituloLG">E-books</h3>
				</div>

				<div class="sliderEbooks col-12 align-self-center anima">
					<div class="seta dir"></div>
					<div class="seta esq"></div>
					<div class="itens">
               		<?php while($queryBook->have_posts()): $queryBook->the_post(); ?>
						<div class="item">
							<div class="conteudo">
		               			<div class="row">
			               			<div class="col-12 col-md-6 order-2 order-md-1 align-self-center">
			               				<a href="<?php the_permalink(); ?>">
											<h4><?php the_title(); ?></h4>
											<?php the_excerpt(); ?>
											<button>Baixe gratuitamente</button>
										</a>
									</div>
			               			<div class="col-12 col-md-6 order-1 order-md-2 align-self-center text-right">
			               				<a href="<?php the_permalink(); ?>">
			               					<?php the_post_thumbnail(); ?>
			               				</a>
			               				<a href="<?php echo home_url('ebooks'); ?>">
			               					<span>Conheça<br> <strong>todos os e-books</strong></span>
			               				</a>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
     <?php endif; ?>

	<section class="newsletter">
		<div class="container">
			<div class="row full">
				<div class="col-12 align-self-center">
					<h3 class="tituloLG esq anima left">News</h3>
					<h3 class="tituloLG dir anima right">Letter</h3>
				</div>
				<div class="col-12 col-md-7 align-self-center text-left anima left">
					<div class="imageLG"></div>
				</div>
				<div class="col-12 col-md-5 align-self-center anima right">
					<h3>Receba as novidades da Oncológica em seu e-mail.</h3>
					<div class="formulario">

						<div id="mauticform_wrapper_newslettersite" class="mauticform_wrapper">
						    <form autocomplete="false" role="form" method="post" action="https://mkt.oncologicamanaus.com.br/form/submit?formId=3" id="mauticform_newslettersite" data-mautic-form="newslettersite" enctype="multipart/form-data">
						        <div class="mauticform-error" id="mauticform_newslettersite_error"></div>
						        <div class="mauticform-message" id="mauticform_newslettersite_message"></div>
						        <div class="mauticform-innerform">

						            
						          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

						            <div id="mauticform_newslettersite_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
						                <input id="mauticform_input_newslettersite_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_newslettersite_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
						                <input id="mauticform_input_newslettersite_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_newslettersite_submit" class="mauticform-row mauticform-button-wrapper mauticform-field-3">

					                	<input type="submit" name="mauticform[submit]" id="mauticform_input_agendamentosite_submit" value="" class="mauticform-button btn btn-default">
						            </div>
						            </div>
						        </div>

						        <input type="hidden" name="mauticform[formId]" id="mauticform_newslettersite_id" value="3">
						        <input type="hidden" name="mauticform[return]" id="mauticform_newslettersite_return" value="">
						        <input type="hidden" name="mauticform[formName]" id="mauticform_newslettersite_name" value="newslettersite">

						        </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="endereco anima">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 align-self-center">
					<h3 class="tituloLG"><img alt="Endereços" title="Endereços" src="<?php echo get_template_directory_uri(); ?>/assets/img/mapa.png" /> Endereços</h3>
					<a href="https://goo.gl/maps/NqXXcbCpnsTjQq7eA" target="_blank">
						<h4>Avenida Jornalista Humberto Calderaro Filho,445<br> Cristal Office Tower, sala -1506 - Adrianópolis</h4>
					</a>
				</div>
			</div>
		</div>
		<div class="mapa container-fluid">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.9651787675184!2d-60.012433285049376!3d-3.1039069411853504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x926c054ce907fdb5%3A0x21d73eec06d70ba3!2sCristal%20Tower!5e0!3m2!1spt-BR!2sbr!4v1627673703340!5m2!1spt-BR!2sbr" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
		</div>
	</section>

<?php get_footer();