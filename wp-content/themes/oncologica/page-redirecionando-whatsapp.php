<!doctype html>
<html <?php language_attributes(); ?>>
     <head>
          <meta charset="<?php bloginfo( 'charset' ); ?>">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="profile" href="https://gmpg.org/xfn/11">
          <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
          <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png">
          <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png">
          <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/site.webmanifest">
          <meta name="msapplication-TileColor" content="#DA532C">
          <meta name="theme-color" content="#FFFFFF">  
          <?php wp_head(); ?>
     </head>
     <body <?php body_class(); ?>>
          <?php wp_body_open(); ?>
          <div id="page" class="site">
               <main id="primary" class="site-main">

                    <section class="obrigado wp">
                         <div class="container">
                              <div class="row justify-content-center">
                                   <div class="col-12 col-lg-6 text-center align-self-center">
                                        <div class="conteudo">
                                             <img class="load" src="<?php echo get_template_directory_uri(); ?>/assets/img/load.gif">
                                             <h2>Você será redirecionado para o WhatsApp<br> da <strong>Oncológica Manaus</strong> em <strong class="contador">3</strong> segundos</h2>
                                             <h3>Caso, não sejá redirecionado, <a href="https://wa.me/5511994537042/">clique aqui</a>.</h3>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </section>

               </main><!-- #main -->
          </div><!-- #page -->
          <?php wp_footer(); ?>
          <script type="text/javascript">
               $(document).ready(function(){
                    var intervalo = 2;
                    setInterval(function(){
                         $('.contador').html(intervalo);
                         intervalo--;
                         if (intervalo == -1) {
                              window.location.href = 'https://wa.me/5592984479000';
                         }
                    }, 1000);
               });
          </script>
     </body>
</html>
