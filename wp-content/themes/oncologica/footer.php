<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Oncológica
 */

?>
</main><!-- #main -->
<footer id="colophon" class="site-footer">
  <div class="sidebar">
    <div class="redes">
      <span><a href="tel:559233456000" target="_blank">
          <img class="rede" alt="Telefone" title="Telefone"
            src="<?php echo get_template_directory_uri(); ?>/assets/img/lateralFone.png" />
        </a>
        <a class="whatstrigger">
          <img class="rede" alt="WhatsApp" title="WhatsApp"
            src="<?php echo get_template_directory_uri(); ?>/assets/img/lateralWhats.png" />
        </a>
        <a target="_blank" href="<?php echo home_url('contato'); ?>">
          <img class="rede" alt="Contato" title="Contato"
            src="<?php echo get_template_directory_uri(); ?>/assets/img/lateralContato.png" />
        </a>
        <a target="_blank" href="https://www.facebook.com/oncologicamanaus">
          <img class="rede" alt="Facebook" title="Facebook"
            src="<?php echo get_template_directory_uri(); ?>/assets/img/lateralFace.png" />
        </a>
        <a target="_blank" href="https://www.instagram.com/oncologiamanaus/">
          <img class="rede" alt="Instagram" title="Instagram"
            src="<?php echo get_template_directory_uri(); ?>/assets/img/lateralInsta.png" />
        </a>
    </div>
  </div>
  <div class="bgCirculo grande"></div>
  <div class="bgCirculo pequeno"></div>
  <div class="container">
    <div class="row justify-content-center">
      <div class="site-info col-12 col-md-6">
        <a href="<?php echo home_url(); ?>" target="_self">
          <img class="logo" alt="Oncológica Manaus" title="Oncológica Manaus"
            src="<?php echo get_template_directory_uri(); ?>/assets/img/logobranco.png" />
        </a>
        <a href="https://goo.gl/maps/3ZCbbPxoev9M6fr96" target="_blank">
          <span><img
              alt="Avenida Jornalista Humberto Calderaro Filho,445 - Cristal Office Tower, sala 1506 - Adrianópolis"
              title="Avenida Jornalista Humberto Calderaro Filho,445 - Cristal Office Tower, sala 1506 - Adrianópolis"
              src="<?php echo get_template_directory_uri(); ?>/assets/img/miniPin.png" />Avenida Jornalista Humberto
            Calderaro Filho,445<br> Cristal Office Tower, sala 1506 - Adrianópolis</span>
        </a>
        <span><a href="tel:559233456000" target="_blank"><img alt="" title=""
              src="<?php echo get_template_directory_uri(); ?>/assets/img/miniPhone.png" /> Tel (92)
            3345-6000</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="whatstrigger"><img alt="" title=""
              src="<?php echo get_template_directory_uri(); ?>/assets/img/miniWhats.png" />(92) 98447-9000</a></span>
        <span><a href="https://www.instagram.com/oncologiamanaus/" target="_blank"><img class="icon-instagram" alt=""
              title="" src="<?php echo get_template_directory_uri(); ?>/assets/img/lateralInsta.png" />
            @oncologiamanaus</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="whatstrigger"
            href="https://www.facebook.com/oncologicamanaus"><img class="icon-facebook" alt="" title=""
              src="<?php echo get_template_directory_uri(); ?>/assets/img/lateralFace.png" />/oncologicamanaus</a></span>
        <a href="mailto:atendimento@oncologicamanaus.com.br" target="_blank">
          <span><img alt="" title=""
              src="<?php echo get_template_directory_uri(); ?>/assets/img/miniMail.png" />atendimento@oncologicamanaus.com.br</span>
        </a>

        <h3>Agende sua consulta <a href="<?php echo home_url('agendamento') ?>">aqui</a>.</h3>
        <p>2021 © Todos os direitos reservados. O conteúdo deste site foi elaborado pela equipe da Clínica Oncológica
          Manaus e as informações aqui contidas têm caráter meramente informativo e educacional. Não deve ser utilizado
          para realizar autodiagnóstico ou automedicação. Em caso de dúvidas, consulte seu médico. Somente ele está
          habilitado a praticar o ato médico, conforme recomendação do Conselho Federal de Medicina. Todas as imagens
          contidas no site são meramente ilustrativas e foram compradas em banco de imagens, portanto não utilizam
          imagens de pacientes. Diretor técnico responsável: Dr. Marcelo Asahiti Uratani | CRM-AM 4894 • RQE 2740 • RQE
          3150</p>
      </div><!-- .site-info -->
    </div>
  </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
<script type="text/javascript">
if (typeof MauticSDKLoaded == 'undefined') {
  var MauticSDKLoaded = true;
  var head = document.getElementsByTagName('head')[0];
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://mkt.oncologicamanaus.com.br/media/js/mautic-form.js';
  script.onload = function() {
    MauticSDK.onLoad();
  };
  head.appendChild(script);
  var MauticDomain = 'https://mkt.oncologicamanaus.com.br';
  var MauticLang = {
    'submittingMessage': "Please wait..."
  }
} else if (typeof MauticSDK != 'undefined') {
  MauticSDK.onLoad();
}
</script>

</html>