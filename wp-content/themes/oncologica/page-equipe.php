<?php get_header(); ?>

<section class="equipe">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 align-self-center text-left">
				<div class="row justify-content-center">
					<div class="col-12 col-md-9 text-center">
						<h3 class="tituloLG esq">Nossa</h3>
						<h3 class="tituloLG dir">equipe</h3>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $queryEqp = new WP_Query(array( 
		'post_type' => 'equipe',
		'posts_per_page' => -1,
		'order' => 'ASC'
	)); ?>
	<div class="container container-destaques">	
	<?php if($queryEqp->have_posts()): ?>	

		<?php $diresq = true; ?>
     	<?php while($queryEqp->have_posts()): $queryEqp->the_post(); ?>
     		<?php $index = $queryEqp->current_post; ?>

	     	<?php if ($index % 2 == 0): ?>
			<div class="row justify-content-<?php echo ($diresq)?'start':'end'; ?>">
				<div class="col-12 col-lg-10">
				<div class="destaques">
				<div class="circulo"></div>
	     	<?php endif; ?>

			
			<a href="<?php the_permalink(); ?>" class="destaque <?php echo ($index % 2 == 0)?'esq':'dir'; ?>">
				<div class="img class">
					<?php //the_post_thumbnail(); ?>
					<div class="wrap" style="background-color: <?php the_field('fundo_thumb'); ?>; background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>
				</div>
				<h2><?php the_field('nome'); ?> <?php the_field('sobrenome'); ?></h2>
				<h3><?php the_field('especialidade'); ?></h3>
				<h4><?php the_field('crm'); ?> • <?php the_field('rqe_1'); if (get_field('rqe_2')){echo ' • '.get_field('rqe_2');} ?></h4>
			</a>

	     	<?php if ($index % 2 == 1): ?>
				<?php $diresq = !$diresq; ?>
				</div>
				</div>
			</div>
     		<?php endif; ?>

		<?php endwhile; ?>
		<?php if ($index % 2 == 1): ?>
			</div>
			</div>
		</div>
		<?php endif; ?>
	<?php endif; ?>
	</div>
</section>

<?php get_footer();