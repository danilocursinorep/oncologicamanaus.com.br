<?php get_header(); ?>
	<?php $author = get_user_by('slug', get_query_var('author_name')); ?>
     <?php $paged = (get_query_var('paged'))?get_query_var('paged'):1; ?>
     <?php $queryPost = new WP_Query(array( 
          'post_type' => 'post',
          'posts_per_page' => 6,
          'order' => 'DESC',
          'author' =>  $author->ID,
          'paged' => $paged 
     )); ?>
     <?php if($queryPost->have_posts()): ?>
	<section class="blog">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-10 align-self-center text-left">
					
					<div class="row justify-content-center">

						<div class="col-12 col-md-9 text-center">
							<h3 class="tituloLG"><?php echo $author->display_name; ?></h3>
						</div>
					</div>

					<div class="itens row justify-content-center">
                    	<?php while($queryPost->have_posts()): $queryPost->the_post(); ?>
                    		<?php $index = $queryPost->current_post + 1; ?>
						<div class="item item<?php echo $index; ?> col-12 col-md-4">
							<a href="<?php the_permalink(); ?>">
								<div class="img">
									<?php the_post_thumbnail(); ?>
								</div>
								<h2><?php the_title(); ?></h2>
							</a>
						</div>
                    	<?php endwhile; ?>
	                         <div class="paginacao col-12">
	                              <?php pagination($queryPost); ?>
	                         </div>
					</div>
				</div>
			</div>
		</div>
	</section>
     <?php endif; ?>

<?php get_footer();