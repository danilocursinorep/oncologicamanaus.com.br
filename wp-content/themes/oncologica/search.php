<?php get_header(); ?>

<section class="equipe">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 align-self-center text-left">
				<div class="row justify-content-center">
					<div class="col-12 col-md-9 text-center">
						<h3 class="tituloLG esq">Resultado</h3>
						<h3 class="tituloLG dir">da pesquisa</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container container-destaques">	
	<?php if(have_posts()): ?>	

		<?php $diresq = true; ?>
		<?php $index = 0; ?>
     	<?php while(have_posts()): the_post(); ?>

	     	<?php if ($index % 2 == 0): ?>
			<div class="row justify-content-<?php echo ($diresq)?'start':'end'; ?>">
				<div class="col-12 col-lg-10">
				<div class="destaques">
				<div class="circulo"></div>
	     	<?php endif; ?>

			
			<a href="<?php the_permalink(); ?>" class="destaque <?php echo ($index % 2 == 0)?'esq':'dir'; ?>">
				<div class="img class">
					<div class="wrap" style="background-image:  url(<?php the_post_thumbnail_url(); ?>)"></div>
				</div>
				<h2><?php the_title(); ?></h2>
			</a>

	     	<?php if ($index % 2 == 1): ?>
				<?php $diresq = !$diresq; ?>
				</div>
				</div>
			</div>
     		<?php endif; ?>

     		<?php $index++; ?>
		<?php endwhile; ?>
		<?php if ($index % 2 == 1): ?>
			</div>
			</div>
		</div>
		<?php endif; ?>
	<?php endif; ?>
	</div>
</section>

<?php get_footer();