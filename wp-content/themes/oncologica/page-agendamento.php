<?php get_header(); ?>

<section class="agendamento">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 align-self-center text-left">
				
				<div class="row justify-content-center">

					<div class="col-12 col-md-9 text-center">
						<h3 class="tituloLG dir">Pré-</h3>
						<h3 class="tituloLG esq">Agendamento</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="img-detacada alt">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/dtAgenda.jpg" />
	</div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 align-self-center text-left">
				
				<div class="row justify-content-center">

					<div class="col-12 col-md-9 text-center formulario alt">
						
						<h3>Um atendimento totalmente humanizado</h3>

						<p>Preencha seus dados e a nossa equipe entrará em contato para efetuar o agendamento.</p>

						<div id="mauticform_wrapper_agendamento2site" class="mauticform_wrapper">
						    <form autocomplete="false" role="form" method="post" action="https://mkt.oncologicamanaus.com.br/form/submit?formId=4" id="mauticform_agendamento2site" data-mautic-form="agendamento2site" enctype="multipart/form-data">
						        <div class="mauticform-error" id="mauticform_agendamento2site_error"></div>
						        <div class="mauticform-message" id="mauticform_agendamento2site_message"></div>
						        <div class="mauticform-innerform">
						            
						            <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">
						                <div id="mauticform_agendamento2site_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
						                    <input id="mauticform_input_agendamento2site_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
						                    <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						                </div>
						                <div id="mauticform_agendamento2site_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
						                    <input id="mauticform_input_agendamento2site_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
						                    <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						                </div>
						                <div id="mauticform_agendamento2site_celular1" data-validate="celular1" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
						                    <input id="mauticform_input_agendamento2site_celular1" name="mauticform[celular1]" value="" placeholder="Celular" class="mauticform-input celular" type="text">
						                    <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						                </div>
						                <div id="mauticform_agendamento2site_submit" class="mauticform-row mauticform-button-wrapper mauticform-field-4">
						                	<input type="submit" name="mauticform[submit]" id="mauticform_input_agendamentosite_submit" value="" class="mauticform-button btn btn-default">
						                </div>
						            </div>
						        </div>
						        <input type="hidden" name="mauticform[formId]" id="mauticform_agendamento2site_id" value="4">
						        <input type="hidden" name="mauticform[return]" id="mauticform_agendamento2site_return" value="">
						        <input type="hidden" name="mauticform[formName]" id="mauticform_agendamento2site_name" value="agendamento2site">
						    </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();