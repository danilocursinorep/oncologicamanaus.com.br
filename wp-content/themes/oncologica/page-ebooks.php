<?php get_header(); ?>

     
<section class="ebooks">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 align-self-center text-left">
				
				<div class="row justify-content-center">

					<div class="col-12 col-md-9 text-center">
						<h3 class="tituloLG">E-books</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="img-detacada alt">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/dtEbooks.jpg" />
	</div>

	<?php $paged = (get_query_var('paged'))?get_query_var('paged'):1; ?>
     <?php $queryPost = new WP_Query(array( 
          'post_type' => 'ebook',
          'posts_per_page' => 2,
          'order' => 'DESC',
          'paged' => $paged 
     )); ?>
     <?php if($queryPost->have_posts()): ?>
	<div class="container">
		<div class="itens row justify-content-center">
     	<?php while($queryPost->have_posts()): $queryPost->the_post(); ?>
     		<?php $index = $queryPost->current_post + 1; ?>
			<div class="item item<?php echo $index; ?> col-12 col-md-8">
				<a href="<?php the_permalink(); ?>">
					<h2><?php the_title(); ?></h2>
					<h3><?php the_field('descricao_curta'); ?></h3>
					<div class="img">
						<?php the_post_thumbnail(); ?>
					</div>
					<button>baixe gratuitamente</button>
				</a>
			</div>
     	<?php endwhile; ?>
               <div class="paginacao col-12">
                    <?php pagination($queryPost); ?>
               </div>
		</div>
	</div>
     <?php endif; ?>
</section>

<?php get_footer();