<?php get_header(); ?>

     
<section class="contato">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 align-self-center text-left">
				
				<div class="row justify-content-center">

					<div class="col-12 col-md-9 text-center">
						<h3 class="tituloLG">Contato</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="img-detacada">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/dtContato.jpg" />
	</div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 align-self-center text-left">
				
				<div class="row justify-content-center">

					<div class="col-12 col-md-9 text-center formulario alt">

						<h3>Fale conosco</h3>

						<p>Deixe uma mensagem no campo abaixo ou utilize nossos canais de comunicação</p>

						<ul>
							<li>
								<a target="_blank" href="https://goo.gl/maps/Ak4FL1LAwjoDUrkM8">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icPin.png" /><!-- 
								 	--><span>Avenida Jornalista Humberto Calderaro Filho,445<br> Cristal Office Tower, sala -1506 -Adrianópolis</span>
								 </a>
							</li>
							<li>
								<a target="_blank" href="tel:559233456000">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icFone.png" /><!-- 
								 	--><span>(92) 3345-6000</span>
								 </a>
							</li>
							<li>
								<a class="whatstrigger">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icWhats.png"/><!-- 
								 	--><span>(92) 98447-9000</span>
								 </a>
							</li>
							<li>
								<a target="_blank" href="mailto:atendimento@oncologicamanaus.com.br">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icMail.png" /><!-- 
								 	--><span>atendimento@oncologicamanaus.com.br</span>
								 </a>
							</li>
						</ul>

						<div id="mauticform_wrapper_contatosite" class="mauticform_wrapper">
						    <form autocomplete="false" role="form" method="post" action="https://mkt.oncologicamanaus.com.br/form/submit?formId=2" id="mauticform_contatosite" data-mautic-form="contatosite" enctype="multipart/form-data">
						        <div class="mauticform-error" id="mauticform_contatosite_error"></div>
						        <div class="mauticform-message" id="mauticform_contatosite_message"></div>
						        <div class="mauticform-innerform">

						            
						          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

						            <div id="mauticform_contatosite_nome" data-validate="nome" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
						                <input id="mauticform_input_contatosite_nome" name="mauticform[nome]" value="" placeholder="Nome" class="mauticform-input" type="text">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_contatosite_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
						                <input id="mauticform_input_contatosite_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_contatosite_celular1" data-validate="celular1" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
						                <input id="mauticform_input_contatosite_celular1" name="mauticform[celular1]" value="" placeholder="Celular" class="mauticform-input celular" type="text">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_contatosite_mensagem" data-validate="mensagem" data-validation-type="textarea" class="mauticform-row mauticform-text mauticform-field-4 mauticform-required">
						                <textarea id="mauticform_input_contatosite_mensagem" name="mauticform[mensagem]" placeholder="Mensagem" class="mauticform-textarea"></textarea>
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_contatosite_submit" class="mauticform-row mauticform-button-wrapper mauticform-field-5">
						            	<input type="submit" name="mauticform[submit]" id="mauticform_input_agendamentosite_submit" value="" class="mauticform-button btn btn-default">
						            </div>
						            </div>
						        </div>

						        <input type="hidden" name="mauticform[formId]" id="mauticform_contatosite_id" value="2">
						        <input type="hidden" name="mauticform[return]" id="mauticform_contatosite_return" value="">
						        <input type="hidden" name="mauticform[formName]" id="mauticform_contatosite_name" value="contatosite">

						        </form>
						</div>



					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();