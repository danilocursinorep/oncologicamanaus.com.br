<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Oncológica
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/site.webmanifest">
	<meta name="msapplication-TileColor" content="#DA532C">
	<meta name="theme-color" content="#FFFFFF">	

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	
	<header id="masthead" class="home-header">
		<img class="topo-img" alt="Pesquisar no site" title="Pesquisar no site" src="<?php echo get_template_directory_uri(); ?>/assets/img/topoSite.png" />
		<?php  get_template_part('template-parts/menu'); ?>
		<div class="txt-topo container">
			<div class="itens">
				<div class="item">
					<div class="row full">
						<div class="content col-6 offset-lg-1 align-self-center">
							<h2>Quando uma equipe multidisciplinar faz<br> a diferença no seu<br> tratamento e<br> recuperação.</h2>
						</div>

					</div>
				</div>
			</div>
		</div>

	</header><!-- #masthead -->

	<main id="primary" class="site-main">
