<?php get_header(); ?>

     
<section class="contato">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 align-self-center text-left">
				
				<div class="row justify-content-center">

					<div class="col-12 col-md-9 text-center">
						<h3 class="tituloLG esq">Muito</h3>
						<h3 class="tituloLG dir">Obrigado</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="img-detacada alt">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/dtAgenda.jpg" />
	</div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 align-self-center text-left">
				
				<div class="row justify-content-center">

					<div class="col-12 col-md-9 text-center formulario alt">

						<h3>Recebemos seus dados</h3>

						<p>Uma de nossas secretárias irá entrar em contato com você para confirmar o melhor dia e horário para a sua consulta.</p>

						<p><strong>ATENÇÃO: Seu pré-agendamento não garante a efetivação da consulta.</strong></p>

						<p>Em caso de dúvida, por favor, entre em contato.</p>

						<br/>

						<ul>
							<li>
								<a href="tel:559233456000">(92) 3345-6000</a>
							</li>
							<li>
								<a class="whatstrigger">(92) 98447-9000</a>
							</li>
							<li>
								<a href="mailto:atendimento@oncologicamanaus.com.br">atendimento@oncologicamanaus.com.br</a>
							</li>
						</ul>


					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();