<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Oncológica
 */

?>

<section id="post-<?php the_ID(); ?>" <?php post_class('pagepost'); ?>>
	<div class="img-detacada">
		<?php the_post_thumbnail(); ?>
	</div>
	<article class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-8 align-self-center">
				<h2 class="titulo"><?php the_title(); ?></h2>
				<h3 class="titulo">Por <?php autores(); ?></h3>
				<div class="content">
					<?php the_content(); ?>
				</div>
				<div class="comments">
					<?php if (comments_open() || get_comments_number()): ?>
						<?php comments_template(); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</article>
</section>

<?php $queryPost = new WP_Query(array( 
     'post_type' => 'post',
     'posts_per_page' => 3,
     'order' => 'DESC'
)); ?>
<?php if($queryPost->have_posts()): ?>
<section class="blogHome">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-9">
				<a href="<?php echo home_url('blog'); ?>">
					<button class="over">Conheça todos os blogposts <img src="<?php echo get_template_directory_uri(); ?>/assets/img/setaSubmit.png" /></button>
				</a>
			</div>
			<div class="col-12 col-lg-10 align-self-center text-left">

				<div class="itens row justify-content-center">

               	<?php while($queryPost->have_posts()): $queryPost->the_post(); ?>
               		<?php $index = $queryPost->current_post + 1; ?>
					<div class="item item<?php echo $index; ?> col-12 col-md-3">
						<a href="<?php the_permalink(); ?>">
							<div class="img">
								<?php the_post_thumbnail(); ?>
							</div>
							<h2><?php the_title(); ?></h2>
						</a>
					</div>
               	<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>