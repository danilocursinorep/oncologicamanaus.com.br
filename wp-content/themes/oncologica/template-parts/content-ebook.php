<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Oncológica
 */

?>

<section id="post-<?php the_ID(); ?>" <?php post_class('bookpost'); ?>>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 align-self-center text-left">
				
				<div class="row justify-content-center">

					<div class="col-12 col-md-9 text-center">

						<h3 class="tituloLG">E-book</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<article class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-8 align-self-center formulario">
				<div class="destacada">
					<?php the_post_thumbnail(); ?>
				</div>
				<h2 class="titulo"><?php the_title(); ?></h2>
				<h3 class="titulo"><?php echo get_field('categoria')->name; ?></h3>
				<div class="content">
					<?php the_field('descricao'); ?>
					<?php the_content(); ?>
				</div>
				<form class="form">
					<input type="text" placeholder="Nome" name="">
					<input type="text" placeholder="E-mail" name="">
					<input class="celular" type="text" placeholder="Celular" name="">
					<select>
						<option>Deseja agendar uma consulta?</option>
					</select>
					<input type="checkbox" name="group1" id="r1" value="1" checked />
					<label for="r1"> Aceito receber esse e-book e outros materiais</label>
					<button type="submit" name="">Baixe gratuitamente</button>
				</form>
			</div>
		</div>
	</article>
</section>