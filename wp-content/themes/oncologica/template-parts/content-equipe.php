<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Oncológica
 */

?>

<section id="post-<?php the_ID(); ?>" <?php post_class('equipost'); ?>>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-10 align-self-center text-left">
				
				<div class="row justify-content-center">

					<div class="col-12 col-md-9 text-center">

						<?php $title = explode('-', get_the_title()); ?>

						<h3 class="tituloLG esq"><?php the_field('nome'); ?></h3>
						<h3 class="tituloLG dir"><?php the_field('sobrenome'); ?></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<article class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-8 align-self-center">
				<?php the_post_thumbnail(); ?>
				<h2 class="titulo"><?php the_field('nome'); ?> <?php the_field('sobrenome'); ?></h2>
				<h3 class="titulo"><?php the_field('crm'); ?> • <?php the_field('rqe_1'); if (get_field('rqe_2')){echo ' • '.get_field('rqe_2');} ?></h3>
				<div class="content">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</article>
</section>