<div class="menuFixo">
     <div class="container">
          <div class="row justify-content-center">

               <nav id="site-navigation" class="main-navigation col-2 col-lg-1 align-self-center">
                    <div id="menu-icone">
                       <span></span>
                       <span></span>
                       <span></span>
                       <span></span>
                   </div>
                    <div class="menu-container">
                    <?php wp_nav_menu(array(
                         'theme_location' => 'menu-1',
                         'menu_id' => 'primary-menu'
                    )); ?>
                    </div>
               </nav><!-- #site-navigation -->
               <div class="site-branding col-8 col-lg-5 align-self-center">
                    <a href="<?php echo home_url(); ?>" target="_self">
                         <img class="logo" alt="Oncológica Manaus" title="Oncológica Manaus" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" />
                    </a>
               </div><!-- .site-branding -->
               <div class="site-search col-2 col-lg-1 align-self-center">
                    <img class="search" alt="Pesquisar no site" title="Pesquisar no site" src="<?php echo get_template_directory_uri(); ?>/assets/img/search.png" />
               </div><!-- .site-search -->

          </div>
     </div>
</div>
<?php get_search_form(); ?>