$(document).ready(function(){

     var sliderHeader = $('.sliderTopo .itens').slick({
          dots: false,
          infinite: true,
          arrows: false,
          speed: 400,
          fade: true,
          autoplay: true,
          autoplaySpeed: 10000
     });

     var sliderEbooks = $('.sliderEbooks .itens').slick({
          dots: false,
          infinite: true,
          arrows: false,
          adaptiveHeight: true
     });

     $('.donut').click(function(){
          sliderHeader.slick('slickNext');
     });

     $('.sliderEbooks .seta.dir').click(function(){
          sliderEbooks.slick('slickNext');
     });
     $('.sliderEbooks .seta.esq').click(function(){
          sliderEbooks.slick('slickPrev');
     });
     
     $('.site-search .search').on('click', function(){
          $('#searchform').addClass('open');
          setTimeout(function() {
               $('#searchform').addClass('show');
          }, 1);
     });
     
     $('#searchform .backclose').on('click', function(){
          $('#searchform').removeClass('show');
          setTimeout(function() {
               $('#searchform').removeClass('open');
          }, 400);
     });


     $('.celular').mask('(00) 00000-0000');
     
     $('#menu-icone').on('click', function(){
          $(this).toggleClass('open');
          if ($('#site-navigation .menu-container').hasClass('show')) {
               $('#site-navigation .menu-container').removeClass('show');
               setTimeout(function() {
                    $('#site-navigation .menu-container').removeClass('open');
               }, 400);
          } else {
               $('#site-navigation .menu-container').addClass('open');
               setTimeout(function() {
                    $('#site-navigation .menu-container').addClass('show');
               }, 1);
          }
     });

     $('li.menu-item-has-children > a').on('click', function(e){
          e.preventDefault();
          $(this).parent().parent('ul').toggleClass('hidden');
          $(this).parent().toggleClass('show');

          if($(this).hasClass('lastMenu')) {
               $(this).removeClass('lastMenu');
               $(this).parent('li').parent('.sub-menu').parent('li').children('a').addClass('lastMenu');
          } else {
               $('.lastMenu').removeClass('lastMenu');
               $(this).addClass('lastMenu');
          }

          $(this).next('.sub-menu').toggleClass('show');
     });
     
     var anima = 0;
     $('.anima').on('inview', function(event, isInView) {
          if ($(this).hasClass('anima')) {
               var fade = $(this).addClass('fadein' + anima);

               if($(this).hasClass('top')) {
                    let animation = anime({
                        targets: '.fadein'+ anima,
                        translateY: [-30,0],
                        opacity: [0,1],
                        easing: "easeInExpo",
                        duration: 1200
                    });
               } else if($(this).hasClass('left')) {
                    let animation = anime({
                        targets: '.fadein'+ anima,
                        translateX: [-30,0],
                        opacity: [0,1],
                        easing: "easeInExpo",
                        duration: 1200
                    });
               } else if($(this).hasClass('right')) {
                    let animation = anime({
                        targets: '.fadein'+ anima,
                        translateX: [30,0],
                        opacity: [0,1],
                        easing: "easeInExpo",
                        duration: 1200
                    });
               } else {
                    let animation = anime({
                        targets: '.fadein'+ anima,
                        translateY: [100,0],
                        opacity: [0,1],
                        easing: "easeInExpo",
                        duration: 1200
                    });
               }
               $(this).removeClass('fadein' + anima);
               $(this).removeClass('anima');
               anima++;
          }
          
     });
     
});
     
$(window).scroll(function(){});


function update(){ 
     var inicial = $(window).scrollTop(); 
     var final = $('#page').height();

     var height = inicial * 100 / final;

     $('#page').css('backgroundPosition', height + '% ' + height + '%'); 
};
$(window).bind('scroll', update);


$('img').on('dragstart', function(event) { event.preventDefault(); });