<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Oncológica
 */

get_header(); ?>

	<?php while (have_posts()): the_post(); ?>
          <?php if (get_post_type() == 'post'): ?>
		   <?php get_template_part('template-parts/content', 'post'); ?>
          <?php elseif (get_post_type() == 'equipe'): ?>
             <?php get_template_part('template-parts/content', 'equipe'); ?>
          <?php elseif (get_post_type() == 'ebook'): ?>
             <?php get_template_part('template-parts/content', 'ebook'); ?>
          <?php endif; ?>
	<?php endwhile; ?>

<?php get_footer();